import React, {Component} from 'react';
import {StyleSheet, View} from 'react-native';
import GDTabView, { GDTabViewProps } from './components/tab-view/tab-view';

const FirstRoute = () => (
  <View style={[styles.scene, { backgroundColor: '#ff4081' }]} />
);
const SecondRoute = () => (
  <View style={[styles.scene, { backgroundColor: '#673ab7' }]} />
);

const tabViewProps: GDTabViewProps = {
  index: 0,
  tabs: [{
    title: "Home",
    key: 'home',
    component: FirstRoute
  },
  {
    title: "About",
    key: 'about',
    component: SecondRoute
  }]
}

interface Props {}
export default class App extends Component<Props> {
  render() {
    return (
      <View style={styles.container}>
        <GDTabView {...tabViewProps}></GDTabView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  scene: {
    flex: 1,
  }
});