import * as React from 'react';
import { Dimensions } from 'react-native';
import { TabView, SceneMap } from 'react-native-tab-view';
 
export interface GDTab {
    title: string;
    key: string;
    component: any;
}

export interface GDTabViewProps {
    tabs: GDTab[];
    index: number | 0;
}

const arrayToObject = (array: Array<GDTab>) =>
   array.reduce((obj: any, item) => {
     obj[item.key] = item.component
     return obj
   }, {})

interface GDTabViewState {
    index: number;
    routes: Array<{title: string, key: string}>;
}

export default class GDTabView extends React.Component<GDTabViewProps, GDTabViewState> {
  state = {
    index: this.props.index,
    routes: this.props.tabs.map(i => {
        return {
            title: i.title,
            key: i.key
        }
    }),
  };
 
  render() {
    return (
      <TabView
        navigationState={this.state}
        renderScene={SceneMap(arrayToObject(this.props.tabs))}
        onIndexChange={index => this.setState({ index })}
        initialLayout={{ width: Dimensions.get('window').width }}
      />
    );
  }
}